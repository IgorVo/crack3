#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be

int compareSort(const void *a, const void *b);
int compareSearch(const void *a, const void *b);

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    // TODO: FILL THIS IN
    char plain[PASS_LEN];
    char hashes[33];
};

typedef struct entry entry;

void printEntry(entry e);

// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
entry *read_dictionary(char *filename, int *size)
{
    FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}

    int count = 0;
    int arrLength = 10;
    entry * arr = malloc(arrLength * sizeof(entry));
    char line[100];

    while (fgets(line, 100, in) != NULL) 
    {
        // Trim newline
        if (strchr(line, '\n') != NULL) 
        {
            *strchr(line, '\n') = '\0';
        }

        if (count == arrLength) 
        {
            arrLength += 10;
            arr = realloc(arr, arrLength * sizeof(entry));
        }

        sscanf(line, "%s", arr[count].plain);
        strcpy(arr[count].hashes,md5(line, strlen(line)));

        count++;
    }
    fclose(in);
    *size = count;
    return arr;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int size;
    entry *dict = read_dictionary(argv[1], &size);
    printf("Loaded in Dictionary\n");
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, size, sizeof(entry), compareSort);
    printf("Sorted Dictionary\n");
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    //entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, size, sizeof(entry), compareSearch);

    
    // TODO
    // Open the hash file for reading.

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    FILE *inn = fopen(argv[2], "r");
	if (!inn)
	{
	    perror("Can't open hash list");
	    exit(1);
	}

    char line[33];
    while (fgets(line, 100, inn) != NULL) 
    {
        // Trim newline
        if (strchr(line, '\n') != NULL) 
        {
            *strchr(line, '\n') = '\0';
        }
        entry *found = bsearch(line, dict, size, sizeof(entry), compareSearch);
        if (found) 
        {
            printEntry(*found);
        }
        else 
        {
            printf("Didn't find hash\n");
        }

    }
    fclose(inn);
    /* Print Dictionary
    for (int i = 0; i < size; i++) 
    {
        printEntry(dict[i]);
    }*/
}

void printEntry(entry e) 
{
    printf("%s %s\n", e.plain, e.hashes);
}

int compareSort(const void *a, const void *b) 
{
  entry *aa = (entry *)a;
  entry *bb = (entry *)b;
  return strcmp(aa->hashes,bb->hashes);
}

int compareSearch(const void *a, const void *b) 
{
    char *aa = (char *)a;
    entry *bb = (entry *)b;
    return strcmp(aa,bb->hashes);
}